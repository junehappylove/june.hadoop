/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.examples.mllib;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.classification.LogisticRegressionModel;
import org.apache.spark.mllib.classification.LogisticRegressionWithSGD;
import org.apache.spark.mllib.evaluation.MulticlassMetrics;
import org.apache.spark.mllib.regression.LabeledPoint;
import org.apache.spark.mllib.util.MLUtils;
// $example off$

// $example on$
import scala.Tuple2;

/**
 * Example for LogisticRegressionWithLBFGS.
 */
public class JavaLogisticRegressioSGDEALL {
	private static String ip = "";
	private static String path = "";

	public static void main(String[] args) throws Exception {
		if (args.length > 0) {
			ip = args[0];
			if (args.length == 2) {
				path = args[1];// /home/iscas/works/sparktestdata/
			} else {
				throw new Exception("error args");
			}

		} else {
			ip = "local";
		}
		SparkConf conf = new SparkConf().setAppName("JavaLogisticRegressioSGDEALL").setMaster("spark://" + ip);
		SparkContext sc = new SparkContext(conf);
		System.out.println("SPARK MASTER:" + ip);
		// $example on$
		String filepath = path + "traindata1.txt";

		JavaRDD<LabeledPoint> data = MLUtils.loadLibSVMFile(sc, filepath).toJavaRDD();

		JavaRDD<LabeledPoint> training = data;
		String testpath0 = path + "1_1826.txt";
		String testpath1 = path + "1_2128.txt";
		String testpath2 = path + "all.txt";

		String testpath3 = path + "1826false.txt";
		String testpath4 = path + "2128false.txt";

		JavaRDD<LabeledPoint> testData0 = MLUtils.loadLibSVMFile(sc, testpath0).toJavaRDD();
		JavaRDD<LabeledPoint> testData1 = MLUtils.loadLibSVMFile(sc, testpath1).toJavaRDD();
		JavaRDD<LabeledPoint> testData2 = MLUtils.loadLibSVMFile(sc, testpath2).toJavaRDD();
		JavaRDD<LabeledPoint> testData3 = MLUtils.loadLibSVMFile(sc, testpath3).toJavaRDD();
		JavaRDD<LabeledPoint> testData4 = MLUtils.loadLibSVMFile(sc, testpath4).toJavaRDD();

		// Building the model
		int numIterations = 100;
		double stepSize = 1;
		double miniBatchFraction = 1.0;
		final LogisticRegressionModel model = LogisticRegressionWithSGD.train(JavaRDD.toRDD(training), numIterations,
				stepSize, miniBatchFraction);

		// *********************************************0
		// Compute raw scores on the test set.
		JavaRDD<Tuple2<Object, Object>> predictionAndLabels0 = testData0
				.map(new Function<LabeledPoint, Tuple2<Object, Object>>() {
					/**
					 * long serialVersionUID
					 */
					private static final long serialVersionUID = 1L;

					public Tuple2<Object, Object> call(LabeledPoint p) {
						Double prediction = model.predict(p.features());
						double[] a = p.features().toArray();
						return new Tuple2<Object, Object>(prediction, p.label());
					}
				});

		// Get evaluation metrics.
		MulticlassMetrics metrics0 = new MulticlassMetrics(predictionAndLabels0.rdd());
		double accuracy0 = metrics0.accuracy();
		System.out.println("Accuracy0 = " + accuracy0);

		// *********************************************1
		JavaRDD<Tuple2<Object, Object>> predictionAndLabels1 = testData1
				.map(new Function<LabeledPoint, Tuple2<Object, Object>>() {
					/**
					 * long serialVersionUID
					 */
					private static final long serialVersionUID = 1L;

					public Tuple2<Object, Object> call(LabeledPoint p) {
						Double prediction = model.predict(p.features());
						double[] a = p.features().toArray();
						return new Tuple2<Object, Object>(prediction, p.label());
					}
				});
		// Get evaluation metrics.
		MulticlassMetrics metrics1 = new MulticlassMetrics(predictionAndLabels1.rdd());
		double accuracy1 = metrics1.accuracy();
		System.out.println("Accuracy1 = " + accuracy1);
		// *********************************************2
		JavaRDD<Tuple2<Object, Object>> predictionAndLabels2 = testData2
				.map(new Function<LabeledPoint, Tuple2<Object, Object>>() {
					/**
					 * long serialVersionUID
					 */
					private static final long serialVersionUID = 1L;

					public Tuple2<Object, Object> call(LabeledPoint p) {
						Double prediction = model.predict(p.features());
						double[] a = p.features().toArray();
						return new Tuple2<Object, Object>(prediction, p.label());
					}
				});
		// Get evaluation metrics.
		MulticlassMetrics metrics2 = new MulticlassMetrics(predictionAndLabels2.rdd());
		double accuracy2 = metrics2.accuracy();
		System.out.println("Accuracy2 = " + accuracy2);

		// *********************************************3
		JavaRDD<Tuple2<Object, Object>> predictionAndLabels3 = testData3
				.map(new Function<LabeledPoint, Tuple2<Object, Object>>() {
					/**
					 * long serialVersionUID
					 */
					private static final long serialVersionUID = 1L;

					public Tuple2<Object, Object> call(LabeledPoint p) {
						Double prediction = model.predict(p.features());
						double[] a = p.features().toArray();
						return new Tuple2<Object, Object>(prediction, p.label());
					}
				});
		// Get evaluation metrics.
		MulticlassMetrics metrics3 = new MulticlassMetrics(predictionAndLabels3.rdd());
		double accuracy3 = metrics3.accuracy();
		System.out.println("Accuracy3 = " + accuracy3);

		// *********************************************4
		JavaRDD<Tuple2<Object, Object>> predictionAndLabels4 = testData4
				.map(new Function<LabeledPoint, Tuple2<Object, Object>>() {
					/**
					 * long serialVersionUID
					 */
					private static final long serialVersionUID = 1L;

					public Tuple2<Object, Object> call(LabeledPoint p) {
						Double prediction = model.predict(p.features());
						double[] a = p.features().toArray();
						return new Tuple2<Object, Object>(prediction, p.label());
					}
				});
		// Get evaluation metrics.
		MulticlassMetrics metrics4 = new MulticlassMetrics(predictionAndLabels4.rdd());
		double accuracy4 = metrics4.accuracy();
		System.out.println("Accuracy4 = " + accuracy4);

		// Save and load model
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		String time = dateFormat.format(now);
		model.save(sc, path + "tmp/javaLogisticRegressionWithSGDEModel_" + time);

		// $example off$

		sc.stop();
	}
}
