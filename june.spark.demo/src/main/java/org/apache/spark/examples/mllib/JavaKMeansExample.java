/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.apache.spark.examples.mllib;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

// $example on$
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.clustering.KMeans;
import org.apache.spark.mllib.clustering.KMeansModel;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
// $example off$

public class JavaKMeansExample {
	private static String ip = "";
	private static String path = "";

	public static void main(String[] args) throws Exception {
		if (args.length > 0) {
			ip = args[0];
			if (args.length == 2) {
				path = args[1];// /home/iscas/works/sparktestdata/
			} else {
				throw new Exception("error args");
			}

		} else {
			ip = "local";
		}
		SparkConf conf = new SparkConf().setAppName("JavaKMeansExample").setMaster("spark://" + ip);
		JavaSparkContext jsc = new JavaSparkContext(conf);

		// $example on$
		// Load and parse data
		String fpath = path + "kmeans_data.txt";
		JavaRDD<String> data = jsc.textFile(fpath);
		JavaRDD<Vector> parsedData = data.map(new Function<String, Vector>() {
			/**
			 * long serialVersionUID
			 */
			private static final long serialVersionUID = 1L;

			public Vector call(String s) {
				String[] sarray = s.split(" ");
				double[] values = new double[sarray.length];
				for (int i = 0; i < sarray.length; i++) {
					values[i] = Double.parseDouble(sarray[i]);
				}
				return Vectors.dense(values);
			}
		});
		parsedData.cache();

		// Cluster the data into two classes using KMeans
		int numClusters = 2;
		int numIterations = 20;
		KMeansModel clusters = KMeans.train(parsedData.rdd(), numClusters, numIterations);

		System.out.println("Cluster centers:");
		for (Vector center : clusters.clusterCenters()) {
			System.out.println(" " + center);
		}
		double cost = clusters.computeCost(parsedData.rdd());
		System.out.println("Cost: " + cost);

		// Evaluate clustering by computing Within Set Sum of Squared Errors
		double WSSSE = clusters.computeCost(parsedData.rdd());
		System.out.println("Within Set Sum of Squared Errors = " + WSSSE);

		// Save and load model
		clusters.save(jsc.sc(), "target/org/apache/spark/JavaKMeansExample/KMeansModel");
		KMeansModel sameModel = KMeansModel.load(jsc.sc(), "target/org/apache/spark/JavaKMeansExample/KMeansModel");
		// $example off$

		jsc.stop();
	}
}
