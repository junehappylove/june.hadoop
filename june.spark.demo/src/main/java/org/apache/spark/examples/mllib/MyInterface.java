package org.apache.spark.examples.mllib;

import java.util.HashMap;
import java.util.Map;

public class MyInterface {

	final static String MyConst = "1";

	public static void main(String[] args) {
		System.out.println(MyInt.in);
		Map<Integer, String> mymap = new HashMap<Integer, String>();
		mymap.put(1, "1");
		mymap.put(2, "2");
		mymap.put(3, "3");
		for (Map.Entry<Integer, String> entry : mymap.entrySet()) {
			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
		}
	}

	private static interface MyInt {
		final int in = 1;
	}
}
