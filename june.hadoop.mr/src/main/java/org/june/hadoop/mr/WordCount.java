/**
 * 中科方德软件有限公司<br>
 * MRSort:com.june.mr.Sort.java
 * 日期:2016年8月2日
 */
package org.june.hadoop.mr;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * 对输入字符串进行字符串统计。输入文件格式是每行一个字符串。 <br>
 * 
 * @author 王俊伟 wjw.happy.love@163.com
 * @date 2016年8月2日 上午11:42:34
 */
public class WordCount {

	/**
	 * 
	 * MAPPER <br>
	 * 
	 * @author 王俊伟 wjw.happy.love@163.com
	 * @date 2016年8月2日 下午12:51:40
	 */
	public static class Map extends Mapper<Object, Text, Text, IntWritable>{
		private static final IntWritable one = new IntWritable(1);
		private Text word = new Text();
		
		@Override
		protected void map(Object key, Text value, Mapper<Object, Text, Text, IntWritable>.Context context)
				throws IOException, InterruptedException {
			String line = value.toString();//一行一个
			word.set(line);
			context.write(word, one);
		}
		
	}
	
	/**
	 * 
	 * REDUCER <br>
	 * 
	 * @author 王俊伟 wjw.happy.love@163.com
	 * @date 2016年8月2日 下午12:58:40
	 */
	public static class Reduce extends Reducer<Text, IntWritable, Text, IntWritable>{
		private IntWritable total = new IntWritable();
		@Override
		protected void reduce(Text key, Iterable<IntWritable> values,
				Reducer<Text, IntWritable, Text, IntWritable>.Context context) throws IOException, InterruptedException {
			int sum = 0;
			Iterator<IntWritable> it = values.iterator();
			while (it.hasNext()) {
				sum += it.next().get();
			}
			total.set(sum);
			context.write(key, total);
		}
	}
	
	public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
		Configuration conf = new Configuration();
		@SuppressWarnings("deprecation")
		Job job = new Job(conf, "SortJob");
		if (args.length != 2) {
			System.err.println("Usage: RemoveRepeat <in> <out>");
			System.exit(2);
		}
		System.out.println(args[0]);
		System.out.println(args[1]);
		job.setJarByClass(WordCount.class);//
		job.setMapperClass(Map.class);
		job.setReducerClass(Reduce.class);
		job.setCombinerClass(Reduce.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));
		System.exit(job.waitForCompletion(true)?0:1);
	}
}
